# Postcard Memories #
## Project done by OCAD U VAlab ##

### Overview ###

This repository contains the prototype of the Postcard Memories project.
Its goal is to provide a medium of exchange for electronic postcards,
allowing the user to create, send and receive postcards to family members,
including media files such as pictures and videos.
Finally, the application provides a Memories Book, which presents a member's postcards as a collection through time.


### Installation ###

Postcard Memories is a web application based on WAMP. The PC that will act as the server should have it installed and the repository should be added to the "www" folder.

It uses a combination of MySQL, PHP, HTML+CSS and JavaScript technologies to deliver its basic functionality, and all dependencies are already present in the folder structure.

There are additional steps to prepare MySQL:

* This prototype uses root/root as username/password for MySQL
* The database and tables should be created with the following script.

CREATE DATABASE `abhijeet` DEFAULT CHARSET latin1;
USE `abhijeet`;
CREATE TABLE `data1` (
   `name` varchar(100),
   `sender` varchar(100),
   `reciv` varchar(100),
   `count` int(11),
   `filename` varchar(100),
   `acc` varchar(100),
   `srno` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `profile` (
   `fname` varchar(1000),
   `lname` varchar(1000),
   `uname` varchar(1000),
   `pass` varchar(1000),
   `email` varchar(1000),
   `inbox` int(11),
   `family` varchar(100),
   `mediasort` varchar(5000)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


Finally, the maximum file upload size variable in the PHP configuration files should be modified to account for pictures and videos which usually run into multiple megabytes.


Notes: 
At this moment, only "jpg" pictures and "mp4", "m4a","3gp" video formats are supported.


### Run ###

* Tablets, smartphones, laptops, PCs and the server must be connected to the same network.

* Turn off the server's Firewall or open the HTTP port 80.

* Turn on WAMP Server on the server PC and make sure its status is online.

* On the clients, open a browser and connect to the chosen server by address name or IP numbers; and add /abhijeet to end.


### Folder Structure ###

The main folder is /abhijeet and within it the /upload folder stores all media and text related files for each postcard.
The /php sub-folder has PHP scripts to interact with the file system and the /js sub-folder contains the external javascript libraries.

All CSS files are inside the /CSS sub-folder, and the mains ones are:

* user.css

* style.css


### Dependencies ###

Postcard Memories uses:

* WAMP Stack.
* JQuery (included).
* Modernizr (included).
* Foundation (included).
* Fastclick (included).
* Font-awesome (included).

### OCAD U specific notes ###
Preliminary steps:

* Ensure all tablets are fully charged

* Clear memory and browser cache

* Don’t use on any tablets except Samsung Android

* For video cameras, be sure to have extra batteries and fully charged


Turn off the server's Firewall:

* From Start menu, open Control Panel

* Click “Windows Firewall”

* On left, click option on left: turn on/ off Windows Firewall

* Turn off for both options on page - need to be off for public & for private network connections


Turn on WAMP Server on laptop:

* Double-click pink WAMP server icon on desktop

* Hit yes to pop-up window that appears

* Icon turns green on bottom toolbar—indicating it’s running 



Start Netgear router:

* Turn on router

* Connect to Netgear79 or Netgear79 5G - both work same

* Enter Password: melodicstar450

* “Limited” connection is default

* Go to Network Settings on laptop (can search for it)

* Right click open network and sharing 

* Click Netgear wifi link  then Click “Details”

* Write down IP address (note: multiple IP addresses display in “Details” panel, if one doesn’t work try others)



Open Postcard Memories website on laptop and tablet(s):

* Open web browser, Google Chrome, enter IP address in address bar (don’t use remembered address as they may not be same)

* Add: abhijeet to end of IP address

* On laptop, assign participant unique & anonymous user ID and assign them to group -  create ID for companion and/or researcher so participants can send/receive postcards

* You don’t need correct info for all “new user” fields - dummy text suffices, password = 1234

* For demos, be sure there is dummy content (e.g., photos, videos, music) loaded

* Lock orientation to landscape (horizontal) mode for optimal viewing

 
Trouble-shooting:

* Make sure PC doesn’t go to sleep - change sleep setting to 30 minutes (or longer)

* If you cannot connect to router, try various IP addresses

* Check router is turned on

* Make sure tablets connected to Netgear network and not another network (tablets may automatically switch to another network)

* Check URL (IP address) that tablet’s browsers are using is correct

* Some media formats won’t attach - may need to use a different content

* Don’t give postcards the same title

* Refresh the app homepage to see new postcards


NOTES: To speed up Lenovo computer, view the following instructions:
https://docs.google.com/document/d/1Uy6bFGhg8LkAW_90-cFynUfZ4imdBIzoSHhOvJyqQIQ/edit?usp=sharing