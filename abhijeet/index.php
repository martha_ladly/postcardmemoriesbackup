<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/user.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/vendor/modernizr.js"></script>

<head>
<title>Postcard Memories | Login</title>
</head> 
<body>
  
 <div class="row">
<!--     <div class="stableBtn small-offset-1 button radius icon-text"><i class="fa fa-chevron-left "></i>back</div>
    <div class="stableBtn home button radius"><i class="fa fa-home"></i></div> -->
     <img class="logo small-offset-9" src="img/logo1.png">
  </div>


<div class="row">
<div class="small-6 small-offset-3 columns">
 <form name="f1" method="post" action="php/authorize.php" id="f1">

  <label>
    <h2>Username:</h2>
    <input type="text" name="uname" value="" required/>
  </label>

  <label>
    <h2>Password:</h2>
    <input type="password" name="pass" value="" required/>
  </label>

  <button type="submit" class="submitBtn button radius icon-text"><i class="fa fa-sign-in"></i>Log In</button>
  
    <a href="/abhijeet/signup.html" class="submitBtn button radius icon-text" style="float:left"><i class="fa fa-sign-in"></i>Sign Up!</a>
  </form> 

  </div>
</div>

<script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/foundation/foundation.magellan.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>