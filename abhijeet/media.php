<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/foundation.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/user.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/vendor/modernizr.js"></script>

<head>

</head> 
<body>
  <div class="row">
    <div class="stableBtn small-offset-1 button radius icon-text"><i class="fa fa-chevron-left "></i>back</div>
    <div class="stableBtn home button radius"><i class="fa fa-home"></i></div>

         <div class="small-9 columns progressBar">
          <h2 class="groupNameBanner">this is the event name</h2>
     <img class="logo small-offset-1" src="img/logo1.png">
  	</div>
</div>


<div class="row">
	<ul class="mediaSelection small-offset-1">
		<li><div class="button radius icon-text"><i class="fa fa-picture-o"></i>picture</div></li>
		<li><div class="button radius icon-text"><i class="fa fa-pencil-square-o"></i>scribble</div></li>
		<li><div class="button radius icon-text"><i class="fa fa-video-camera"></i>video</div></li>
		<li><div class="button radius icon-text"><i class="fa fa-music"></i>music</div></li>
		<li><div class="button radius icon-text"><i class="fa fa-microphone"></i>record</div></li>
	</ul>


	<div class="mediaContent small-offset-2"></div>
	<div class="attachBtn button radius icon-text right"><i class="fa fa-paperclip"></i>attach</div>
	


</div>

<<!-- div class="row">
	<div class="small-offset-9">
	<div class="attachBtn button radius icon-text"><i class="fa fa-paperclip"></i>attach</div>
</div>


<div>
 -->


		<script src="js/vendor/jquery.js"></script>
		<script src="js/vendor/fastclick.js"></script>
		<script src="js/foundation.min.js"></script>

	</body>
	</html>