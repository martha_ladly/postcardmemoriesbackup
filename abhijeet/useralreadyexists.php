<?php

echo '
		<!DOCTYPE html><head>
		<html class="no-js" lang="en" >
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/user.css">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/vendor/modernizr.js"></script>
		</head>
		<body>
		<div class="row" style="text-align:center">
				
				<div style="text-align:center; width:100%;">
					<p class="confirmContent" style="text-align:center">That user-name already exists, please try another.</p>
				
			    	<a href="/abhijeet/signup.html"><div class="confirmSentBtn button radius icon-text" style="width:40%; text-align:center"><i class="fa fa-angle-double-right" style="text-align:left">Click here to go to sign-up page.</i></div></a>
				</div>
		</div>
		</body></html>';
?>		